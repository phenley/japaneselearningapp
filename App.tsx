import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
import { EasyPeasyConfig, Store, StoreProvider } from 'easy-peasy';
import { store } from './src/scripts/modules/store';
import { theme } from './src/scripts/modules/theme';
import { HomeScreen } from './src/scripts/components/HomeScreen';

interface AppProps {
  store: Store<{}, EasyPeasyConfig<undefined, {}>>;
}

export const AppProvider: React.FC<AppProps> = ({ store, children }) => (
  <StoreProvider store={store}>
    <ApplicationProvider {...eva} theme={theme}>
      {children}
    </ApplicationProvider>
  </StoreProvider>
);

export default () => (
  <AppProvider store={store}>
    <HomeScreen />
  </AppProvider>
);
