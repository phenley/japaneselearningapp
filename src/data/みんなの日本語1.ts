export const みんなの日本語1 = [
  {
    id: 'みんなの日本語1.1',
    jmdictId: '1311110',
    kana: [{ text: 'わたし' }],
    kanji: [{ text: '私' }],
    sense: [{ gloss: [{ text: 'I' }, { text: 'me' }], info: ['slightly formal or feminine'], partOfSpeech: ['pn'] }],
  },
  {
    id: 'みんなの日本語1.2',
    jmdictId: '1223615',
    kana: [{ text: 'あなた' }],
    kanji: [{ text: '貴方' }],
    sense: [
      {
        gloss: [{ text: 'you' }],
        misc: ['uk', 'pol'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.3',
    jmdictId: '1000440',
    kana: [{ text: 'あのひと' }],
    kanji: [{ text: 'あの人' }],
    sense: [
      {
        gloss: [{ text: 'he' }, { text: 'she' }, { text: 'that person' }],
        info: ["sometimes of one's spouse or partner"],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.4',
    jmdictId: '1005340',
    kana: [{ text: 'さん' }],
    sense: [
      {
        gloss: [{ text: 'Mr.' }, { text: 'Mrs.' }, { text: 'Miss' }, { text: 'Ms.' }],
        info: ["after a person's name (or position, etc.)"],
        misc: ['hon', 'fam'],
        partOfSpeech: ['suf'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.5',
    jmdictId: '1007660',
    kana: [{ text: 'ちゃん' }],
    sense: [{ gloss: [{ text: 'suffix for familiar person' }], misc: ['fam'], partOfSpeech: ['suf'] }],
  },
  {
    id: 'みんなの日本語1.6',
    jmdictId: '1366410',
    kana: [{ text: 'じん' }],
    kanji: [{ text: '人' }],
    sense: [
      {
        gloss: [{ text: '-ian (e.g. Italian)' }, { text: '-ite (e.g. Tokyoite)' }],
        info: ['indicates nationality, race, origin, etc.'],
        partOfSpeech: ['suf'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.7',
    jmdictId: '1387990',
    kana: [{ text: 'せんせい' }],
    kanji: [{ text: '先生' }],
    sense: [{ gloss: [{ text: 'instructor' }, { text: 'master' }, { text: 'teacher' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語1.8',
    jmdictId: '1237130',
    kana: [{ text: 'きょうし' }],
    kanji: [{ text: '教師' }],
    sense: [{ gloss: [{ text: 'teacher (classroom)' }], partOfSpeech: ['n', 'adjNo'] }],
  },
  {
    id: 'みんなの日本語1.9',
    jmdictId: '1206900',
    kana: [{ text: 'がくせい' }],
    kanji: [{ text: '学生' }],
    sense: [{ gloss: [{ text: 'student' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語1.10',
    jmdictId: '1198560',
    kana: [{ text: 'かいしゃいん' }],
    kanji: [{ text: '会社員' }],
    sense: [
      {
        gloss: [{ text: 'company employee' }, { text: 'office worker' }, { text: 'white-collar worker' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.11',
    jmdictId: '1322670',
    kana: [{ text: 'しゃいん' }],
    kanji: [{ text: '社員' }],
    sense: [
      {
        gloss: [{ text: 'member of a corporation' }, { text: 'Hitashi　の　～' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.12',
    kana: [{ text: 'ぎんこういん' }],
    kanji: [{ text: '銀行員' }],
    sense: [
      {
        gloss: [{ text: 'bank employee' }, { text: 'banker' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.13',
    jmdictId: '1159980',
    kana: [{ text: 'いしゃ' }],
    kanji: [{ text: '医者' }],
    sense: [
      {
        gloss: [{ text: '(medical) doctor' }, { text: 'physician' }],
        info: ['お医者さん is polite'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.14',
    jmdictId: '1258590',
    kana: [{ text: 'けんきゅうしゃ' }],
    kanji: [{ text: '研究者' }],
    sense: [{ gloss: [{ text: 'researcher' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語1.15',
    jmdictId: '1413240',
    kana: [{ text: 'だいがく' }],
    kanji: [{ text: '大学' }],
    sense: [{ gloss: [{ text: 'university' }, { text: 'college' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語1.16',
    jmdictId: '1490220',
    kana: [{ text: 'びょういん' }],
    kanji: [{ text: '病院' }],
    sense: [{ gloss: [{ text: 'hospital' }], partOfSpeech: ['n', 'adjNo'] }],
  },
  {
    id: 'みんなの日本語1.17',
    jmdictId: '1416830',
    kana: [{ text: 'だれ' }],
    kanji: [{ text: '誰' }],
    sense: [{ gloss: [{ text: 'who' }], partOfSpeech: ['pn'] }],
  },
  {
    id: 'みんなの日本語1.18',
    jmdictId: '1294940',
    kana: [{ text: 'さい' }],
    kanji: [{ text: '歳' }, { text: '才' }],
    sense: [{ gloss: [{ text: 'years old' }], partOfSpeech: ['suf', 'ctr'] }],
  },
  {
    id: 'みんなの日本語1.19',
    jmdictId: '1219960',
    kana: [{ text: 'おいくつ' }],
    sense: [{ gloss: [{ text: 'how old?' }], partOfSpeech: ['adv'] }],
  },
  {
    id: 'みんなの日本語1.20',
    jmdictId: '1010080',
    kana: [{ text: 'はい' }],
    sense: [{ gloss: [{ text: 'yes' }, { text: 'that is correct' }], misc: ['pol'], partOfSpeech: ['int'] }],
  },
  {
    id: 'みんなの日本語1.21',
    jmdictId: '1583250',
    kana: [{ text: 'いいえ' }],
    kanji: [{ text: '否' }],
    sense: [{ gloss: [{ text: 'no' }, { text: 'nay' }], misc: ['uk'], partOfSpeech: ['int'] }],
  },
  {
    id: 'みんなの日本語1.22',
    kana: [{ text: 'はじめまして' }],
    kanji: [{ text: '初めまして' }],
    sense: [{ gloss: [{ text: 'Nice to meet you' }] }],
    partOfSpeech: ['exp'],
  },
  {
    id: 'みんなの日本語1.23',
    jmdictId: '1224890',
    kana: [{ text: 'よろしく' }],
    kanji: [{ text: '宜しく' }, { tags: ['ateji'], text: '宜敷く' }],
    sense: [
      {
        gloss: [{ text: 'best regards' }, { text: 'please treat me favorably' }],
        partOfSpeech: ['exp', 'adv'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.24',
    jmdictId: '1320230',
    kana: [{ text: 'しつれいですが' }],
    kanji: [{ text: '失礼ですが' }],
    sense: [
      {
        gloss: [{ text: 'excuse me, but...' }],
        partOfSpeech: ['exp'],
      },
    ],
  },
  {
    id: 'みんなの日本語1.25',
    jmdictId: '1531710',
    kana: [{ text: 'なまえ' }],
    kanji: [{ text: '名前' }],
    sense: [{ gloss: [{ text: 'name' }], partOfSpeech: ['n'] }],
  },
];
