export const みんなの日本語2 = [
  {
    id: 'みんなの日本語2.1',
    jmdictId: '1628530',
    kana: [{ text: 'これ' }],
    kanji: [{ text: '此れ' }],
    sense: [
      {
        gloss: [
          { text: 'this (indicating an item near the speaker, the action of the speaker, or the current topic)' },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.2',
    jmdictId: '1006970',
    kana: [{ text: 'それ' }],
    kanji: [{ text: '其れ' }],
    sense: [
      {
        gloss: [
          {
            text: 'that (indicating an item or person near the listener, the action of the listener, or something on their mind)',
          },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.3',
    jmdictId: '1000580',
    kana: [{ text: 'あれ' }],
    kanji: [{ text: '彼' }],
    sense: [
      {
        gloss: [
          {
            text: 'that (indicating something distant from both speaker and listener (in space, time or psychologically), or something understood without naming it directly)',
          },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.4',
    jmdictId: '1582920',
    kana: [{ text: 'この' }],
    kanji: [{ text: '此の' }],
    sense: [
      {
        gloss: [{ text: 'this' }],
        info: ['something or someone close to the speaker (including the speaker), or ideas expressed by the speaker'],
        misc: ['uk'],
        partOfSpeech: ['adjPn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.5',
    jmdictId: '1006830',
    kana: [{ text: 'その' }],
    kanji: [{ text: '其の' }],
    sense: [
      {
        gloss: [{ text: 'that' }],
        info: [
          'something or someone distant from the speaker, close to the listener; actions of the listener, or ideas expressed or understood by the listener',
        ],
        misc: ['uk'],
        partOfSpeech: ['adjPn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.6',
    jmdictId: '1000420',
    kana: [{ text: 'あの' }],
    kanji: [{ text: '彼の' }],
    sense: [
      {
        gloss: [{ text: 'that' }],
        info: [
          'someone or something distant from both speaker and listener, or situation unfamiliar to both speaker and listener',
        ],
        misc: ['uk'],
        partOfSpeech: ['adjPn'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.7',
    jmdictId: '1522150',
    kana: [{ text: 'ほん' }],
    kanji: [{ text: '本' }],
    sense: [
      {
        gloss: [{ text: 'book' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.8',
    jmdictId: '1318970',
    kana: [{ text: 'じしょ' }],
    kanji: [{ text: '辞書' }],
    sense: [{ gloss: [{ text: 'dictionary' }, { text: 'lexicon' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.9',
    jmdictId: '1299400',
    kana: [{ text: 'ざっし' }],
    kanji: [{ text: '雑誌' }],
    sense: [{ gloss: [{ text: 'magazine' }, { text: 'journal' }, { text: 'periodical' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.10',
    jmdictId: '1362360',
    kana: [{ text: 'しんぶん' }],
    kanji: [{ text: '新聞' }],
    sense: [{ gloss: [{ text: 'newspaper' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.11',
    jmdictId: '1093450',
    kana: [{ text: 'ノート' }],
    sense: [
      {
        gloss: [{ text: 'notebook' }, { text: 'copy-book' }, { text: 'exercise book' }],
        misc: ['abbr'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.12',
    jmdictId: '1598330',
    kana: [{ text: 'てちょう' }],
    kanji: [{ text: '手帳' }],
    sense: [
      {
        gloss: [{ text: 'personal organiser' }, { text: 'notebook' }, { text: 'memo pad' }, { text: '(pocket) diary' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.13',
    jmdictId: '1531550',
    kana: [{ text: 'めいし' }],
    kanji: [{ text: '名刺' }],
    sense: [{ gloss: [{ text: 'business card' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.14',
    jmdictId: '1036400',
    kana: [{ text: 'カード' }],
    sense: [{ gloss: [{ text: 'card' }, { text: 'credit card' }], partOfSpeech: ['n'] }],
  },

  {
    id: 'みんなの日本語2.15',
    jmdictId: '1178590',
    kana: [{ text: 'えんぴつ' }],
    kanji: [{ text: '鉛筆' }],
    sense: [{ gloss: [{ text: 'pencil' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.16',
    jmdictId: '1123590',
    kana: [{ text: 'ボールペン' }],
    sense: [
      {
        gloss: [{ text: 'ball-point pen' }],
        languageSource: [{ text: 'ball pen', wasei: true }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.17',
    jmdictId: '1061300',
    kana: [{ text: 'シャープペンシル' }],
    sense: [
      {
        gloss: [{ text: 'propelling pencil' }, { text: 'mechanical pencil' }],
        languageSource: [{ text: 'sharp pencil', wasei: true }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.18',
    jmdictId: '1260490',
    kana: [{ text: 'かぎ' }],
    kanji: [{ text: '鍵' }],
    sense: [{ gloss: [{ text: 'key' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.19',
    jmdictId: '1316140',
    kana: [{ text: 'とけい' }],
    kanji: [{ common: true, text: '時計' }],
    sense: [{ gloss: [{ text: 'clock' }, { text: 'watch' }, { text: 'timepiece' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.20',
    jmdictId: '1301940',
    kana: [{ text: 'かさ' }],
    kanji: [{ text: '傘' }],
    sense: [{ gloss: [{ text: 'umbrella' }, { text: 'parasol' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.21',
    jmdictId: '1208910',
    kana: [{ text: 'かばん' }],
    kanji: [{ text: '鞄' }],
    sense: [
      {
        gloss: [{ text: 'bag' }, { text: 'satchel' }, { text: 'briefcase' }, { text: 'basket' }],
        misc: ['uk'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.22',
    jmdictId: '1080510',
    kana: [{ text: 'テレビ' }],
    sense: [
      {
        gloss: [{ text: 'television' }, { text: 'TV' }],
        misc: ['abbr'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.23',
    jmdictId: '1138860',
    kana: [{ text: 'ラジオ' }],
    sense: [{ gloss: [{ text: 'radio' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.24',
    jmdictId: '1038350',
    kana: [{ text: 'カメラ' }],
    sense: [{ gloss: [{ text: 'camera' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.25',
    jmdictId: '1053350',
    kana: [{ text: 'コンピューター' }],
    sense: [{ gloss: [{ text: 'computer' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.26',
    jmdictId: '1323080',
    kana: [{ text: 'くるま' }],
    kanji: [{ text: '車' }],
    sense: [{ gloss: [{ text: 'car' }, { text: 'automobile' }, { text: 'vehicle' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.27',
    jmdictId: '1220210',
    kana: [{ text: 'つくえ' }],
    kanji: [{ text: '机' }],
    sense: [{ gloss: [{ text: 'desk' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.28',
    jmdictId: '1157070',
    kana: [{ text: 'いす' }],
    kanji: [{ text: '椅子' }],
    sense: [{ gloss: [{ text: 'chair' }, { text: 'stool' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.29',
    jmdictId: '1078270',
    kana: [{ text: 'チョコレート' }],
    sense: [{ field: ['food'], gloss: [{ text: 'chocolate' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.30',
    jmdictId: '1049180',
    kana: [{ text: 'コーヒー' }],
    kanji: [{ tags: ['ateji'], text: '珈琲' }],
    sense: [
      {
        gloss: [{ text: 'coffee' }],
        languageSource: [{ text: 'coffee' }, { lang: 'dut', text: 'koffie' }],
        misc: ['uk'],
        partOfSpeech: ['n', 'adjNo'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.31',
    jmdictId: '1002500',
    kana: [{ text: 'おみやげ' }],
    kanji: [{ text: 'お土産' }],
    sense: [
      {
        gloss: [
          { text: 'local specialty or souvenir bought as a gift while travelling' },
          { text: 'present brought by a visitor' },
        ],
        misc: ['pol'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語2.32',
    jmdictId: '1174420',
    kana: [{ text: 'えいご' }],
    kanji: [{ text: '英語' }],
    sense: [{ gloss: [{ text: 'English (language)' }], partOfSpeech: ['n', 'adjNo'] }],
  },
  {
    id: 'みんなの日本語2.33',
    jmdictId: '1464530',
    kana: [{ text: 'にほんご' }],
    kanji: [{ text: '日本語' }],
    sense: [{ gloss: [{ text: 'Japanese (language)' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語2.34',
    jmdictId: '2846738',
    kana: [{ text: 'なん' }],
    kanji: [{ text: '何' }],
    sense: [{ gloss: [{ text: 'what' }], partOfSpeech: ['pn'] }],
  },
  {
    id: 'みんなの日本語2.35',
    jmdictId: '2137720',
    kana: [{ text: 'そう' }],
    kanji: [{ text: '然う' }],
    sense: [
      {
        gloss: [{ text: 'so' }, { text: 'in that way' }, { text: 'thus' }, { text: 'such' }],
        info: [
          'used to express agreement with something said',
          "concerning the actions of the listener or concerning the ideas expressed or understood by the listener; with a neg. sentence, implies that something isn't as much as one might think",
        ],
        misc: ['uk'],
        partOfSpeech: ['adv'],
      },
    ],
  },
];
