import { Button, Text, useTheme } from '@ui-kitten/components';
import React from 'react';
import { Planet } from 'react-kawaii/lib/native/';
import { View } from 'react-native';
import { getWordDescription, getWordKanjiList, Word } from '../modules/dictionary';
import { t } from 'i18n-js';
import { CardFooter } from './styled/CardFooter';
import { Character } from './styled/Character';
import { CorrectText } from './styled/CorrectText';
import { WrongText } from './styled/WrongText';
import { css } from '@emotion/native';
import { CardHeader } from './styled/CardHeader';
import { CardBody } from './styled/CardBody';
import { Card } from './styled/Card';

interface FailProps {
  word: Word;
  lastAnswer: string;
  proceedToNextWord: () => void;
}

export const Fail: React.FC<FailProps> = ({ word, lastAnswer, proceedToNextWord }) => {
  const theme = useTheme();
  const description = getWordDescription(word.id);
  const kanjiList = getWordKanjiList(word.id);

  return (
    <Card status="danger">
      <CardHeader>
        <Text category="h6">{description}</Text>
      </CardHeader>
      <CardBody>
        <Character>
          <Planet size={200} mood="ko" color={theme['color-danger-default']} />
        </Character>
        <View
          style={css`
            flex-direction: row;
            justify-content: space-between;
          `}
        >
          <Text>{kanjiList}</Text>
          <WrongText>{lastAnswer}</WrongText>
        </View>
        <CorrectText category="h1">{word.kana[0].text}</CorrectText>
      </CardBody>
      <CardFooter>
        <Button onPress={proceedToNextWord} testID={'continue-button'}>
          {t('result.continue')}
        </Button>
      </CardFooter>
    </Card>
  );
};
