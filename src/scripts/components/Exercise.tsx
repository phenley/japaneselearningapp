import * as React from 'react';
import { View } from 'react-native';
import { Word } from '../modules/dictionary';
import { ChoiceAnswer } from './ChoiceAnswer';
import { TextAnswer } from './TextAnswer';

interface ExerciseProps {
  onSuccess: () => void;
  onRescue: () => void;
  onFail: () => void;
  setLastAnswer: (word: string) => void;
  word: Word;
}

export const Exercise: React.FC<ExerciseProps> = ({ onSuccess, onRescue, onFail, setLastAnswer, word }) => {
  const [attemptIndex, setAttemptIndex] = React.useState(0);

  const onWrongAnswer = () => {
    const nextAttemptIndex = attemptIndex + 1;

    if (nextAttemptIndex === exercises.length) {
      onFail();
      setAttemptIndex(0);
    } else {
      setAttemptIndex(nextAttemptIndex);
    }
  };

  const onRightAnswer = () => {
    if (attemptIndex === 0) {
      onSuccess();
    } else {
      onRescue();
    }

    setAttemptIndex(0);
  };

  const exercises = [
    <TextAnswer onWrongAnswer={onWrongAnswer} onRightAnswer={onRightAnswer} word={word} />,
    <ChoiceAnswer
      word={word}
      setLastAnswer={setLastAnswer}
      onWrongAnswer={onWrongAnswer}
      onRightAnswer={onRightAnswer}
    />,
  ];

  if (!word) {
    return <View testID="word-missing" />;
  }

  return <>{exercises[attemptIndex]}</>;
};
