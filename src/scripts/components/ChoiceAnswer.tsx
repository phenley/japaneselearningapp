import React from 'react';
import { RadioGroup, Radio, Button, Text, useTheme } from '@ui-kitten/components';
import { getRandomWord, getWordDescription, Word } from '../modules/dictionary';
import { Planet } from 'react-kawaii/lib/native/';
import { t } from '../modules/translation';
import { CardFooter } from './styled/CardFooter';
import { Character } from './styled/Character';
import { Card } from './styled/Card';
import { CardHeader } from './styled/CardHeader';
import { CardBody } from './styled/CardBody';

interface ChoiceAnswerProps {
  word: Word;
  onWrongAnswer: () => void;
  onRightAnswer: () => void;
  setLastAnswer: (word: string) => void;
}

export const ChoiceAnswer: React.FC<ChoiceAnswerProps> = ({ word, onWrongAnswer, onRightAnswer, setLastAnswer }) => {
  const [selectedIndex, setSelectedIndex] = React.useState<number>(null);
  const theme = useTheme();

  const getRandomOptionItem = () => getRandomWord().kana[0].text;

  const getRandomOptionItems = (amount: number) => {
    const answer = word.kana[0].text;
    const options = [];

    while (amount !== options.length) {
      const option = getRandomOptionItem();

      if (option !== answer && !options.includes(option)) {
        options.push(option);
      }
    }

    options.splice(Math.floor(amount * Math.random()), 0, answer);

    return options;
  };

  const [options] = React.useState(getRandomOptionItems(3));

  return (
    <Card status="warning">
      <CardHeader>
        <Text category="h6">{getWordDescription(word.id)}</Text>
      </CardHeader>
      <CardBody>
        <Character>
          <Planet size={200} mood="shocked" color={theme['color-warning-default']} />
        </Character>
        <RadioGroup selectedIndex={selectedIndex} onChange={(index) => setSelectedIndex(index)}>
          {options.map((option) => (
            <Radio key={option} testID={`radio-${option}`}>
              {option}
            </Radio>
          ))}
        </RadioGroup>
      </CardBody>
      <CardFooter>
        <Button
          disabled={selectedIndex === null}
          onPress={() => {
            if (options[selectedIndex] === word.kana[0].text) {
              onRightAnswer();
            } else {
              onWrongAnswer();
              setLastAnswer(options[selectedIndex]);
            }
          }}
          testID={'confirm-button'}
        >
          {t('exercise.confirm')}
        </Button>
      </CardFooter>
    </Card>
  );
};
