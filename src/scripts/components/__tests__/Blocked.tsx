import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import { createStore, EasyPeasyConfig, Store } from 'easy-peasy';

import { AppProvider } from '../../../../App';
import { getRandomWord } from '../../modules/dictionary';
import { StoreModel, storeModel } from '../../modules/store';
import { Blocked } from '../Blocked';

const proceedToNextWord = jest.fn();

const props = {
  proceedToNextWord,
};

const word = getRandomWord();
let store: Store<StoreModel, EasyPeasyConfig<undefined, {}>>;

const timeout = 200;

beforeEach(() => {
  store = createStore(storeModel);
  store.getActions().addWord(word);
});

describe('Blocked', () => {
  it('should wait for next available word', async () => {
    store.getActions().updateWord({ ...store.getState().words[0], progress: 10 });

    render(
      <AppProvider store={store}>
        <Blocked {...props} />
      </AppProvider>,
    );

    await expect(waitFor(() => expect(proceedToNextWord).toHaveBeenCalled(), { timeout })).rejects.toThrow();
  });

  it('should proceed to next available word', async () => {
    render(
      <AppProvider store={store}>
        <Blocked {...props} />
      </AppProvider>,
    );

    await waitFor(() => expect(proceedToNextWord).toHaveBeenCalled(), { timeout });
  });
});
