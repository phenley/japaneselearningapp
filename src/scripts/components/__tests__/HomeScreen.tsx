import React from 'react';
import { act, render, waitFor, waitForElementToBeRemoved } from '@testing-library/react-native';
import { HomeScreen } from '../HomeScreen';
import { AppProvider } from '../../../../App';
import { createStore } from 'easy-peasy';
import { findNewWord, getRandomWord } from '../../modules/dictionary';
import { storeModel } from '../../modules/store';
import { Success } from '../Success';
import { Fail } from '../Fail';
import { Blocked } from '../Blocked';
import { Rescue } from '../Rescue';
import { mockComponent } from '../__mocks__/mockComponent';
import { Exercise } from '../Exercise';

const [MockedSuccess, getMockedSuccessProps] = mockComponent<typeof Success>();
const [MockedFail, getMockedFailProps] = mockComponent<typeof Fail>();
const [MockedBlocked, getMockedBlockedProps] = mockComponent<typeof Blocked>();
const [MockedRescue, getMockedRescueProps] = mockComponent<typeof Rescue>();
const [MockedExercise, getMockedExerciseProps] = mockComponent<typeof Exercise>();

let config: ReturnType<typeof setup>;

beforeEach(() => {
  config = setup();
});

describe('HomeScreen', () => {
  it('should pass current word to Exercise', async () => {
    render(
      <AppProvider store={config.store}>
        <HomeScreen />
      </AppProvider>,
    );

    await waitFor(() => {
      expect(getMockedExerciseProps().word).toBeDefined();
    });
  });

  describe('when onFail callback has been called', () => {
    it('should render component Fail', async () => {
      render(
        <AppProvider store={config.store}>
          <HomeScreen />
        </AppProvider>,
      );

      await waitFor(() => {
        expect(getMockedExerciseProps()).toBeDefined();
      });

      act(() => {
        getMockedExerciseProps().onFail();
      });

      expect(getMockedFailProps()).toBeDefined();
    });

    it('should lower word progress on fail', async () => {
      const { store } = config;

      render(
        <AppProvider store={store}>
          <HomeScreen />
        </AppProvider>,
      );

      act(() => {
        store.getActions().updateWord({ ...store.getState().words[0], progress: 1 });
      });

      await waitFor(() => {
        expect(store.getState().words[0].progress).toEqual(1);
      });

      act(() => {
        getMockedExerciseProps().onFail();
      });

      expect(store.getState().words[0].progress).toEqual(0);
    });

    it('should not lower word progress below 0', async () => {
      const { store } = config;

      render(
        <AppProvider store={config.store}>
          <HomeScreen />
        </AppProvider>,
      );

      act(() => {
        store.getActions().updateWord({ ...store.getState().words[0], progress: 0 });
      });

      await waitFor(() => {
        expect(store.getState().words[0].progress).toEqual(0);
      });

      act(() => {
        getMockedExerciseProps().onFail();
      });

      expect(store.getState().words[0].progress).toEqual(0);
    });
  });

  describe('when onSuccess callback has been called', () => {
    it('should render component Success', async () => {
      render(
        <AppProvider store={config.store}>
          <HomeScreen />
        </AppProvider>,
      );

      await waitFor(() => {
        expect(getMockedExerciseProps()).toBeDefined();
      });

      act(() => {
        getMockedExerciseProps().onSuccess();
      });

      expect(getMockedSuccessProps()).toBeDefined();
    });

    it('should raise word progress on success', async () => {
      render(
        <AppProvider store={config.store}>
          <HomeScreen />
        </AppProvider>,
      );

      await waitFor(() => {
        expect(getMockedExerciseProps()).toBeDefined();
      });

      act(() => {
        getMockedExerciseProps().onSuccess();
      });

      expect(config.store.getState().words[0].progress).toEqual(1);
    });
  });

  describe('when onRescue callback has been called', () => {
    it('should render component Rescue', async () => {
      render(
        <AppProvider store={config.store}>
          <HomeScreen />
        </AppProvider>,
      );

      await waitFor(() => {
        expect(getMockedExerciseProps()).toBeDefined();
      });

      act(() => {
        getMockedExerciseProps().onRescue();
      });

      expect(getMockedRescueProps()).toBeDefined();
    });

    it('should not increment or decrement word progress', async () => {
      const { store } = config;

      render(
        <AppProvider store={store}>
          <HomeScreen />
        </AppProvider>,
      );

      act(() => {
        store.getActions().updateWord({ ...store.getState().words[0], progress: 10 });
      });

      await waitFor(() => {
        expect(store.getState().words[0].progress).toEqual(10);
      });

      act(() => {
        getMockedExerciseProps().onRescue();
      });

      expect(store.getState().words[0].progress).toEqual(10);
    });
  });

  describe('when no more words are available', () => {
    it('should render component Blocked', async () => {
      const { store } = config;

      store.getActions().updateWord({ ...store.getState().words[0], progress: 10 });

      const {} = render(
        <AppProvider store={store}>
          <HomeScreen />
        </AppProvider>,
      );

      await waitFor(() => {
        expect(getMockedBlockedProps()).toBeDefined();
      });
    });
    describe('when attempting to load a new word', () => {
      describe('when a word has become available', () => {
        it('should render component Exercise', async () => {
          const { store } = config;

          store.getActions().updateWord({ ...store.getState().words[0], progress: 10 });

          const { getByTestId } = render(
            <AppProvider store={store}>
              <HomeScreen />
            </AppProvider>,
          );

          store.getActions().updateWord({ ...store.getState().words[0], progress: 0 });

          act(() => {
            getMockedBlockedProps().proceedToNextWord();
          });

          await waitForElementToBeRemoved(() => getByTestId('mocked-blocked'));

          getByTestId('mocked-exercise');
        });
      });
      describe('when no word has become available', () => {
        it('should not render component Exercise', async () => {
          const { store } = config;

          store.getActions().updateWord({ ...store.getState().words[0], progress: 10 });

          const { getByTestId } = render(
            <AppProvider store={store}>
              <HomeScreen />
            </AppProvider>,
          );

          act(() => {
            getMockedBlockedProps().proceedToNextWord();
          });

          await expect(waitFor(() => getByTestId('mocked-exercise'), { timeout: 100 })).rejects.toThrow();
        });
      });
    });
  });
});

function setup() {
  const word = getRandomWord();
  const store = createStore(storeModel);
  store.getActions().addWord(word);
  return { word, store };
}

jest.mock('../Success', () => ({
  Success: (props) => <MockedSuccess testID="mocked-success" {...props} />,
}));

jest.mock('../Fail', () => ({
  Fail: (props) => <MockedFail testID="mocked-fail" {...props} />,
}));

jest.mock('../Blocked', () => ({
  Blocked: (props) => <MockedBlocked testID="mocked-blocked" {...props} />,
}));

jest.mock('../Rescue', () => ({
  Rescue: (props) => <MockedRescue testID="mocked-rescue" {...props} />,
}));

jest.mock('../Exercise', () => ({
  Exercise: (props) => <MockedExercise testID="mocked-exercise" {...props} />,
}));

jest.mock('../../modules/dictionary', () => {
  return {
    ...jest.requireActual('../../modules/dictionary'),
    findNewWord: () => {
      return undefined;
    },
  };
});
