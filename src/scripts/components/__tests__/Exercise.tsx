import React from 'react';
import { act, render } from '@testing-library/react-native';
import { createStore } from 'easy-peasy';
import { AppProvider } from '../../../../App';
import { getRandomWord } from '../../modules/dictionary';
import { storeModel } from '../../modules/store';
import { Exercise } from '../Exercise';
import { TextAnswer } from '../TextAnswer';
import { ChoiceAnswer } from '../ChoiceAnswer';
import { mockComponent } from '../__mocks__/mockComponent';

const [MockedTextAnswer, getMockedTextAnswerProps] = mockComponent<typeof TextAnswer>();
const [MockedChoiceAnswer, getMockedChoiceAnswerProps] = mockComponent<typeof ChoiceAnswer>();

let config: ReturnType<typeof setup>;
beforeEach(() => {
  config = setup();
});

describe('Exercise', () => {
  describe('when not provided with a word', () => {
    it('should render null', async () => {
      const { findByTestId } = render(
        <AppProvider store={null}>
          <Exercise {...config.props} word={null} />
        </AppProvider>,
      );
      await findByTestId('word-missing');
    });
  });

  describe('when first answer is wrong', () => {
    it('should pass last answer props to its children', () => {
      render(
        <AppProvider store={config.store}>
          <Exercise {...config.props} />
        </AppProvider>,
      );

      act(() => {
        getMockedTextAnswerProps().onWrongAnswer();
      });

      expect(getMockedChoiceAnswerProps().setLastAnswer).toEqual(config.props.setLastAnswer);
    });

    describe('and second answer is wrong', () => {
      it('should call fail callback', () => {
        render(
          <AppProvider store={config.store}>
            <Exercise {...config.props} />
          </AppProvider>,
        );

        act(() => {
          getMockedTextAnswerProps().onWrongAnswer();
        });

        act(() => {
          getMockedChoiceAnswerProps().onWrongAnswer();
        });

        expect(config.props.onFail).toHaveBeenCalled();
      });
    });

    describe('and second answer is right', () => {
      it('should call rescue callback', () => {
        render(
          <AppProvider store={config.store}>
            <Exercise {...config.props} />
          </AppProvider>,
        );

        act(() => {
          getMockedTextAnswerProps().onWrongAnswer();
        });

        act(() => {
          getMockedChoiceAnswerProps().onRightAnswer();
        });

        expect(config.props.onRescue).toHaveBeenCalled();
      });
    });
  });

  describe('when first answer is right', () => {
    it('should call success callback', () => {
      render(
        <AppProvider store={config.store}>
          <Exercise {...config.props} />
        </AppProvider>,
      );

      act(() => {
        getMockedTextAnswerProps().onRightAnswer();
      });

      expect(config.props.onSuccess).toHaveBeenCalled();
    });
  });
});

function setup() {
  const word = getRandomWord();
  const store = createStore(storeModel);

  store.getActions().addWord(word);

  return {
    store,
    props: {
      onSuccess: jest.fn(),
      onRescue: jest.fn(),
      onFail: jest.fn(),
      setLastAnswer: jest.fn(),
      word,
    },
  };
}

jest.mock('../TextAnswer', () => ({
  TextAnswer: (props) => <MockedTextAnswer {...props} />,
}));

jest.mock('../ChoiceAnswer', () => ({
  ChoiceAnswer: (props) => <MockedChoiceAnswer {...props} />,
}));
