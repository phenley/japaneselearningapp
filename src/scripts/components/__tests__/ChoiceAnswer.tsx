import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import { getRandomWord } from '../../modules/dictionary';
import { ChoiceAnswer } from '../ChoiceAnswer';
import { createStore } from 'easy-peasy';
import { storeModel } from '../../modules/store';
import { AppProvider } from '../../../../App';

jest.mock('../../modules/dictionary');

const word = getRandomWord();
const onWrongAnswer = jest.fn();
const onRightAnswer = jest.fn();
const setLastAnswer = jest.fn();

const props = {
  word,
  onWrongAnswer,
  onRightAnswer,
  setLastAnswer,
};

const store = createStore(storeModel);
store.getActions().addWord(word);

describe('ChoiceAnswer', () => {
  it('should handle right answer properly', () => {
    const { getByTestId } = render(
      <AppProvider store={store}>
        <ChoiceAnswer {...props} />
      </AppProvider>,
    );

    const rightAnswer = getByTestId(`radio-${word.kana[0].text}`);
    fireEvent.press(rightAnswer);

    const confirmButton = getByTestId('confirm-button');
    fireEvent.press(confirmButton);

    expect(onRightAnswer).toHaveBeenCalled();
  });

  test('should handle wrong answer properly', () => {
    const { getAllByTestId, getByTestId } = render(
      <AppProvider store={store}>
        <ChoiceAnswer {...props} />
      </AppProvider>,
    );

    const wrongAnswers = getAllByTestId(new RegExp(`radio-(?!${word.kana[0].text})`));
    fireEvent.press(wrongAnswers[0]);

    const confirmButton = getByTestId('confirm-button');
    fireEvent.press(confirmButton);

    expect(onWrongAnswer).toHaveBeenCalled();
    expect(setLastAnswer).toHaveBeenCalled();
  });
});

afterEach(() => {
  (jest.requireMock('../../modules/dictionary').getRandomWord as jest.Mock).mockClear();
});
