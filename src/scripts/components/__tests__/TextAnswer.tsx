import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import { TextAnswer } from '../TextAnswer';
import * as dictionary from '../../modules/dictionary';
import { getRandomWord, Word } from '../../modules/dictionary';
import { createStore } from 'easy-peasy';
import { storeModel } from '../../modules/store';
import { AppProvider } from '../../../../App';

let config: ReturnType<typeof setup>;

beforeEach(() => {
  config = setup();
});

describe('TextAnswer', () => {
  describe('when the word contains information', () => {
    it('should render this information', () => {
      jest.spyOn(dictionary, 'getWordInfos').mockImplementationOnce(() => {
        return ["Programmer's fuel"];
      });

      const { getByText } = render(
        <AppProvider store={config.store}>
          <TextAnswer {...config.props} word={config.props.word} />
        </AppProvider>,
      );

      expect(getByText("Programmer's fuel")).toBeDefined();
    });
  });

  describe('when "Confirm" button pressed', () => {
    describe('when the answer is right', () => {
      it('should call the onRightAnswer callback', () => {
        const { getByTestId } = render(
          <AppProvider store={config.store}>
            <TextAnswer {...config.props} />
          </AppProvider>,
        );

        const input = getByTestId('text-input');
        fireEvent.changeText(input, config.props.word.kana[0].text);

        const confirmButton = getByTestId('confirm-button');
        fireEvent.press(confirmButton);

        check();
        expect(config.props.onRightAnswer).toHaveBeenCalled();
      });

      describe('even when provided with some katakanas', () => {
        it('should call the onRightAnswer callback', () => {
          const word: Word = {
            id: 'word-id',
            kana: [{ text: 'コーヒー' }],
            sense: [
              {
                gloss: [{ text: 'coffee' }],
                info: ["Programmer's fuel"],
              },
            ],
          };

          const store = createStore(storeModel);
          store.getActions().addWord(word);
          const { getByTestId } = render(
            <AppProvider store={store}>
              <TextAnswer {...config.props} word={word} />
            </AppProvider>,
          );

          const input = getByTestId('text-input');
          fireEvent.changeText(input, word.kana[0].text);

          const confirmButton = getByTestId('confirm-button');
          fireEvent.press(confirmButton);

          expect(config.props.onRightAnswer).toHaveBeenCalled();
        });
      });
    });

    describe('when the answer is wrong', () => {
      it('should call the onWrongAnswer callback', () => {
        const { getByTestId } = render(
          <AppProvider store={config.store}>
            <TextAnswer {...config.props} />
          </AppProvider>,
        );

        const input = getByTestId('text-input');
        fireEvent.changeText(input, '@');

        const confirmButton = getByTestId('confirm-button');
        fireEvent.press(confirmButton);

        expect(config.props.onWrongAnswer).toHaveBeenCalled();
      });
    });
  });

  describe('when "I don\'t know" button pressed', () => {
    it('should call the onWrongAnswer callback', () => {
      const { getByTestId } = render(
        <AppProvider store={config.store}>
          <TextAnswer {...config.props} />
        </AppProvider>,
      );

      const iDontKnowButton = getByTestId('i-dont-know-button');
      fireEvent.press(iDontKnowButton);

      expect(config.props.onWrongAnswer).toHaveBeenCalled();
    });
  });

  describe('when "Enter" key is pressed', () => {
    describe('when the answer is right', () => {
      it('should call the onRightAnswer callback', () => {
        const { getByTestId } = render(
          <AppProvider store={config.store}>
            <TextAnswer {...config.props} />
          </AppProvider>,
        );

        const input = getByTestId('text-input');
        fireEvent.changeText(input, config.props.word.kana[0].text);

        fireEvent(input, 'onKeyPress', {
          nativeEvent: {
            key: 'Enter',
            keyCode: 13,
          },
        });
        check();
        expect(config.props.onRightAnswer).toHaveBeenCalled();
      });
    });

    describe('when the answer is wrong', () => {
      it('should call the onWrongAnswer callback', () => {
        const { getByTestId } = render(
          <AppProvider store={config.store}>
            <TextAnswer {...config.props} />
          </AppProvider>,
        );

        const input = getByTestId('text-input');
        fireEvent.changeText(input, '@');

        fireEvent(input, 'onKeyPress', {
          nativeEvent: {
            key: 'Enter',
            keyCode: 13,
          },
        });

        expect(config.props.onWrongAnswer).toHaveBeenCalled();
      });
    });

    describe('when the answer field is empty', () => {
      it('should not call any callback', () => {
        const { getByTestId } = render(
          <AppProvider store={config.store}>
            <TextAnswer {...config.props} />
          </AppProvider>,
        );

        const input = getByTestId('text-input');

        fireEvent(input, 'onKeyPress', {
          nativeEvent: {
            key: 'Enter',
            keyCode: 13,
          },
        });

        expect(config.props.onRightAnswer).not.toHaveBeenCalled();
        expect(config.props.onWrongAnswer).not.toHaveBeenCalled();
      });
    });
  });

  describe('when other than "Enter" key is pressed', () => {
    it('should not call any callback', () => {
      const { getByTestId } = render(
        <AppProvider store={config.store}>
          <TextAnswer {...config.props} />
        </AppProvider>,
      );

      const input = getByTestId('text-input');

      fireEvent(input, 'onKeyPress', {
        nativeEvent: {
          key: 'kKey',
          keyCode: 13,
        },
      });

      expect(config.props.onRightAnswer).not.toHaveBeenCalled();
      expect(config.props.onWrongAnswer).not.toHaveBeenCalled();
    });
  });
});

function check() {
  if (config.props.onRightAnswer.mock.calls.length === 0) {
    console.log({ inputedWord: config.props.word.kana });
  }
}

function setup() {
  const word = getRandomWord();
  const onWrongAnswer = jest.fn();
  const onRightAnswer = jest.fn();

  const props = {
    word,
    onWrongAnswer,
    onRightAnswer,
  };

  const store = createStore(storeModel);
  store.getActions().addWord(word);

  return {
    store,
    props,
  };
}
