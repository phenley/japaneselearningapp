import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import { createStore } from 'easy-peasy';
import { AppProvider } from '../../../../App';
import { getRandomWord } from '../../modules/dictionary';
import { storeModel } from '../../modules/store';
import { Rescue } from '../Rescue';

let config: ReturnType<typeof setup>;

beforeEach(() => {
  config = setup();
});

describe('Rescue', () => {
  it('should render right answer', () => {
    const { getByText } = render(
      <AppProvider store={config.store}>
        <Rescue {...config.props} />
      </AppProvider>,
    );
    getByText(config.props.word.kana[0].text);
  });

  it('should render a kanji when available', () => {
    const { getByText } = render(
      <AppProvider store={config.store}>
        <Rescue {...config.props} />
      </AppProvider>,
    );
    getByText('mocked-kanji');
  });

  it('should provide a way to proceed to the next word', () => {
    const { getByTestId } = render(
      <AppProvider store={config.store}>
        <Rescue {...config.props} />
      </AppProvider>,
    );

    fireEvent.press(getByTestId('continue-button'));
    expect(config.props.proceedToNextWord).toHaveBeenCalled();
  });
});

function setup() {
  const proceedToNextWord = jest.fn();

  const word = getRandomWord();
  word.kanji = [
    {
      common: true,
      text: 'mocked-kanji',
    },
  ];

  const store = createStore(storeModel);
  store.getActions().addWord(word);

  return {
    store,
    props: {
      word,
      proceedToNextWord,
    },
  };
}
