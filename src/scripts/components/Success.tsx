import React from 'react';
import { Button, Text, useTheme } from '@ui-kitten/components';
import { getWordDescription, getWordKanjiList, Word } from '../modules/dictionary';
import { Planet } from 'react-kawaii/lib/native/';
import { t } from '../modules/translation';
import { CardFooter } from './styled/CardFooter';
import { Character } from './styled/Character';
import { CorrectText } from './styled/CorrectText';
import { Card } from './styled/Card';
import { CardHeader } from './styled/CardHeader';
import { CardBody } from './styled/CardBody';

interface SuccessProps {
  word: Word;
  proceedToNextWord: () => void;
}

export const Success: React.FC<SuccessProps> = ({ word, proceedToNextWord }) => {
  const theme = useTheme();
  const description = getWordDescription(word.id);
  const kanjiList = getWordKanjiList(word.id);

  return (
    <Card status="success">
      <CardHeader>
        <Text category="h6">{description}</Text>
      </CardHeader>
      <CardBody>
        <Character>
          <Planet size={200} mood="lovestruck" color={theme['color-success-default']} />
        </Character>
        <Text>{kanjiList}</Text>
        <CorrectText category="h1" status="success">
          {word.kana[0].text}
        </CorrectText>
      </CardBody>
      <CardFooter>
        <Button onPress={proceedToNextWord} testID="continue-button">
          {t('result.continue')}
        </Button>
      </CardFooter>
    </Card>
  );
};
