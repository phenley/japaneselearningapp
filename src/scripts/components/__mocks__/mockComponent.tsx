import React from 'react';
import { View } from 'react-native';

function mockComponent<T extends React.ComponentType>(): [
  React.FC<React.ComponentProps<T> & { testID: string }>,
  () => React.ComponentProps<T>,
] {
  type MockedComponentProps = React.ComponentProps<T> & { testID: string };
  let mockComponentProps: MockedComponentProps;

  const MockedComponent: React.FC<MockedComponentProps> = (props) => {
    React.useEffect(() => {
      mockComponentProps = props;
    });
    return <View {...props} />;
  };

  return [MockedComponent, () => mockComponentProps];
}

export { mockComponent };
