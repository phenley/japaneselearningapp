import styled from '@emotion/native';
import { EvaStatus } from '@ui-kitten/components/devsupport';
import { theme } from '../../modules/theme';

interface CardProps {
  status?: EvaStatus;
}

export const Card = styled.View<CardProps>`
  flex-grow: 1;
  flex-shrink: 0;
  background-color: ${theme['background-basic-color-1']};
  border: 1px solid ${theme['border-basic-color-4']};
  border-radius: 4px;
  border-top-width: 5px;
  border-top-color: ${({ status }) => theme[`color-${status || 'primary'}-default`]};
`;
