import styled from '@emotion/native';
import { theme } from '../../modules/theme';

export const CardBody = styled.View`
  flex-grow: 1;
  flex-shrink: 0;
  justify-content: center;
  padding: 16px 24px;
  border: 0 solid ${theme['border-basic-color-4']};
  border-top-width: 1px;
  border-bottom-width: 1px;
`;
