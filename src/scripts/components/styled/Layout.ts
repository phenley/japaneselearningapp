import styled from '@emotion/native';

export const Layout = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  background-color: ${(props) => props.theme['background-basic-color-2']};
`;
