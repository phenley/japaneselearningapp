import styled from '@emotion/native';
import { Text } from '@ui-kitten/components';
import { theme } from '../../modules/theme';

export const WrongText = styled(Text)`
  color: ${theme['color-danger-default']};
  text-decoration-line: line-through;
`;
