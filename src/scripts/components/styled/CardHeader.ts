import styled from '@emotion/native';

export const CardHeader = styled.View`
  padding: 16px 24px;
  flex-shrink: 0;
`;
