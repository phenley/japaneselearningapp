import { words } from '../../data';

export { words };

export type Kana = { appliesToKanji?: string[]; common?: boolean; text: string };

export type Kanji = { common?: boolean; text: string };

export type Gloss = { text: string; type?: string };

export type Related = string[];

export type Sense = { gloss: Gloss[]; info?: string[]; partOfSpeech?: string[]; related?: Related[] };

export type Word = { id: string; kana: Kana[]; kanji?: Kanji[]; sense: Sense[] };

export const findWord = (id: string) => words.find((word) => word.id === id);

export const findNewWord = (ids: string[]) => words.find((word) => !ids.includes(word.id));

export const getRandomWord = () => words[Math.floor(words.length * Math.random())];

export const getWordDescription = (id: string) => {
  const description = words
    .find((word) => word.id === id)
    ?.sense.map((s) => s.gloss.map((g) => g.text).join(', '))
    .join();
  return `${description?.charAt(0).toUpperCase()}${description?.slice(1)}`;
};

export const getWordInfos = (id: string) => {
  const word = words.find((word) => word.id === id);
  const infos = word?.sense.reduce((i: string[], s) => {
    return [...i, ...(s?.info || [])];
  }, []);
  return infos?.map((i) => `${i?.charAt(0).toUpperCase()}${i?.slice(1)}`);
};

export const getWordKanjiList = (id: string) => {
  return words
    .find((word) => word.id === id)
    .kanji?.map((k) => k.text)
    .join(', ');
};
