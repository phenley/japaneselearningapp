export enum ExerciseStatus {
  SUCCESS = 'SUCCESS',
  RESCUE = 'RESCUE',
  FAIL = 'FAIL',
  PENDING = 'PENDING',
  BlOCKED = 'BLOCKED',
}
