import { t } from 'i18n-js';
import { translations } from '../translation';

describe('translation', () => {
  describe('t', () => {
    it('should always return the proper localized string', () => {
      Object.keys(translations.en).forEach((key) => {
        expect(t(key)).toEqual(translations.en[key]);
      });
    });
  });
});
