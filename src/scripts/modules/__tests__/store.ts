import { createStore } from 'easy-peasy';
import { findNewWord, getRandomWord } from '../dictionary';
import { storeModel } from '../store';

jest.mock('../dictionary');

let config: ReturnType<typeof setup>;

beforeEach(() => {
  config = setup();
});

describe('store', () => {
  describe('when created', () => {
    it('should not contain any words', () => {
      expect(config.store.getState().words).toEqual([]);
    });
  });

  describe('pendingWord', () => {
    describe('when a pending word is available', () => {
      it('should return this word', () => {
        config.store.getActions().addWord(config.firstWord);
        const addedWord = config.store.getState().words[0];

        expect(config.store.getState().pendingWord).toEqual(addedWord);
      });
    });
    describe('when no pending word is available', pending);
    describe('when the store contains no words', () => {
      it('should return undefined', () => {
        expect(config.store.getState().pendingWord).toBeUndefined();
      });
    });
  });

  describe('addWord', () => {
    describe('when passed a word', () => {
      it('should assign a progress and a lastEncounterDate to this word and store it', () => {
        config.store.getActions().addWord(config.firstWord);
        const addedWord = config.store.getState().words[0];

        expect(addedWord).toBeDefined();
        expect(addedWord.id).toEqual(config.firstWord.id);
        expect(addedWord.progress).toEqual(0);
        expect(addedWord.lastEncounterDate).toBeDefined();
      });
    });
  });

  describe('getNextWordDueDate', () => {
    describe('when the store contains one word', () => {
      it("should return this word's next due date", () => {
        config.store.getActions().addWord(config.firstWord);

        expect(config.store.getState().getNextWordDueDate()).toEqual(
          config.store.getState().getWordDueDate(config.firstWord.id),
        );
      });
    });
    describe('when the store contains more than one word', pending);
    describe('when the store contains no words', () => {
      it('should return Infinity', () => {
        expect(config.store.getState().getNextWordDueDate()).toEqual(Infinity);
      });
    });
  });

  describe('getNextWord', () => {
    it('should', async () => {
      const nextWord = await config.store.getActions().getNextWord();
      expect(nextWord).toBeDefined();
    });
  });

  describe('getWordDueDate', () => {
    it('should return the correct due date', async () => {
      config.store.getActions().addWord(config.firstWord);
      const word = config.store.getState().words[0];

      const spacingModifier = 0.1;
      const spacing = Math.round((((Math.exp(word.progress * spacingModifier * 0.8934) - 1) * 2) / 3) * 3600000);

      await expect(config.store.getState().getWordDueDate(word.id)).toEqual(word.lastEncounterDate + spacing);
    });
  });

  describe('updateWord', () => {
    it('should find the word with the same id and overite it with the new word', () => {
      config.store.getActions().addWord(config.firstWord);
      const addedWord = config.store.getState().words[0];
      const date = Date.now() + 9999;
      const updatedWordData = { ...addedWord, lastEncounterDate: date, progress: 10 };

      config.store.getActions().updateWord(updatedWordData);

      expect(config.store.getState().words[0]).toEqual(updatedWordData);
    });
  });

  describe('incrementWordProgress', () => {
    it('should incremeent word progress', () => {
      config.store.getActions().addWord(config.firstWord);
      config.store.getActions().incrementWordProgress(config.firstWord.id);
      const addedWord = config.store.getState().words[0];

      expect(addedWord.progress).toEqual(1);
    });
  });

  describe('decrementWordProgress', () => {
    describe('when progress equals 0', () => {
      it('should not decrement word progress', () => {
        config.store.getActions().addWord(config.firstWord);
        config.store.getActions().decrementWordProgress(config.firstWord.id);
        const addedWord = config.store.getState().words[0];

        expect(addedWord.progress).toEqual(0);
      });
    });

    describe('when progress is greater that 0', () => {
      it('should decrement word progress', () => {
        config.store.getActions().addWord(config.firstWord);
        config.store.getActions().incrementWordProgress(config.firstWord.id);
        const incrementedWord = config.store.getState().words[0];

        expect(incrementedWord.progress).toEqual(1);

        config.store.getActions().decrementWordProgress(config.firstWord.id);
        const decrementedWord = config.store.getState().words[0];

        expect(decrementedWord.progress).toEqual(0);
      });
    });
  });

  describe('refreshLastEncounterDate', () => {
    it('should refresh lastEncounteredDate with the current date', async () => {
      config.store.getActions().addWord(config.firstWord);
      const addedWordLastEncounterDate = config.store.getState().words[0].lastEncounterDate;

      await waitTenMs();
      const savedDate = Date.now();

      expect(addedWordLastEncounterDate).toBeLessThan(savedDate);

      await waitTenMs();

      config.store.getActions().refreshLastEncounterDate(config.firstWord.id);
      const refreshedWordLastEncounterDate = config.store.getState().words[0].lastEncounterDate;

      expect(refreshedWordLastEncounterDate).toBeGreaterThan(savedDate);
    });
  });
});

afterEach(() => {
  jest.clearAllMocks();
});

function waitTenMs() {
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve();
    }, 10);
  });
}

function setup() {
  const store = createStore(storeModel);
  const firstWord = getRandomWord();
  const secondWord = findNewWord([firstWord.id]);
  return { store, firstWord, secondWord };
}
