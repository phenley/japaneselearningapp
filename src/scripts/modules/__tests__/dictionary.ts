import { findNewWord, findWord, getRandomWord, getWordDescription, getWordInfos, words } from '../dictionary';

const randomWord = getRandomWord();

describe('dictionary', () => {
  describe('findWord', () => {
    it('should return a the wanted word', () => {
      expect(findWord(randomWord.id)).toEqual(randomWord);
    });
  });

  describe('findNewWord', () => {
    it('should return a word that is not already included in passed word array', () => {
      const [onlyWordLeft, ...alreadytakenWords] = words;
      const foundWord = findNewWord(alreadytakenWords.map((w) => w.kana[0].text));

      expect(foundWord).toEqual(onlyWordLeft);
    });
  });

  describe('getRandomWord', () => {
    it('should return a random word', () => {
      expect(randomWord).toBeDefined();
    });
  });

  describe('getWordDescription', () => {
    it('should return the correct word description', async () => {
      await expect(getWordDescription(words[0].id)).toEqual('I, me');
    });
  });

  describe('getWordInfos', () => {
    it('should return the correct word information string', async () => {
      await expect(getWordInfos(words[0].id)).toEqual(['Slightly formal or feminine']);
    });
  });
});
