import { msToTime } from '../msToTime';

describe('msToTime', () => {
  it('should always return the right time string', () => {
    expect([1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000].map(msToTime)).toEqual([
      '00:00:01',
      '00:00:10',
      '00:01:40',
      '00:16:40',
      '02:46:40',
      '03:46:40',
      '13:46:40',
      '17:46:40',
    ]);
  });
});
