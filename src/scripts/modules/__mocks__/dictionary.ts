module.exports = {
  ...jest.requireActual('../../modules/dictionary'),
  getRandomWord: jest.fn(() => {
    const actualModule = jest.requireActual('../../modules/dictionary');

    const returnWords = [
      actualModule.words[0],
      actualModule.words[1],
      actualModule.words[2],
      actualModule.words[0],
      actualModule.words[1],
      actualModule.words[3],
      actualModule.words[4],
      actualModule.words[5],
      actualModule.words[6],
      actualModule.words[7],
      actualModule.words[8],
      actualModule.words[9],
    ];

    return returnWords[jest.requireMock('../../modules/dictionary').getRandomWord.mock.calls.length - 1];
  }),
};
