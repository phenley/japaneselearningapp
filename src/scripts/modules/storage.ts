import AsyncStorage from '@react-native-async-storage/async-storage';

export const storage = {
  getItem: async (key: string) => JSON.parse(await AsyncStorage.getItem(key)),
  setItem: async (key: string, data: any) => AsyncStorage.setItem(key, JSON.stringify(data)),
  removeItem: async (key: string) => AsyncStorage.removeItem(key),
};
