import { Action, action, computed, Computed, createStore, createTypedHooks, persist, thunk, Thunk } from 'easy-peasy';
import { Word, findNewWord } from './dictionary';
import { storage } from './storage';

export interface WordModel {
  id: string;
  progress: number;
  lastEncounterDate: number;
}

export interface StoreModel {
  words: WordModel[];
  pendingWord: Computed<{ currentDate: number } & StoreModel, WordModel | undefined>;
  addWord: Action<StoreModel, Word>;
  getNextWordDueDate: Computed<StoreModel, () => number>;
  getNextWord: Thunk<StoreModel, undefined, undefined, StoreModel, Promise<WordModel>>;
  getWordDueDate: Computed<StoreModel, (id: string) => number>;
  updateWord: Action<StoreModel, WordModel>;
  incrementWordProgress: Action<StoreModel, string>;
  decrementWordProgress: Action<StoreModel, string>;
  refreshLastEncounterDate: Action<StoreModel, string>;
}

export const storeModel: StoreModel = {
  words: [],
  pendingWord: computed([(state) => state, () => ({ currentDate: Date.now() })], (state, state2) => {
    const getNextWordDueDate = state.getNextWordDueDate;

    if (getNextWordDueDate() > state2.currentDate) {
      return;
    }

    return state.words.find((word) => state.getWordDueDate(word.id) === getNextWordDueDate());
  }),
  addWord: action((state, word) => {
    const spacingModifier = 1;

    state.words.push({
      id: word.id,
      progress: 0,
      lastEncounterDate: Date.now(),
    });
  }),
  getNextWordDueDate: computed((state) => () => Math.min(...state.words.map((word) => state.getWordDueDate(word.id)))),
  getNextWord: thunk(async (actions, _dispatch, { getState }) => {
    const pendingWord = getState().pendingWord;

    if (pendingWord) {
      return pendingWord;
    }

    const newWord = findNewWord(getState().words.map((word) => word.id));

    if (!newWord) {
      return;
    }

    actions.addWord(newWord);

    return getState().pendingWord;
  }),
  getWordDueDate: computed((state) => {
    const spacingModifier = 0.1;
    return (id) => {
      const word = state.words.find((word) => word.id === id);
      const spacing = Math.round((((Math.exp(word.progress * spacingModifier * 0.8934) - 1) * 2) / 3) * 3600000);
      return word.lastEncounterDate + spacing;
    };
  }),
  updateWord: action((state, word) => {
    state.words = [...state.words.filter((w) => w.id !== word.id), word];
  }),
  incrementWordProgress: action((state, id) => {
    const foundWord = { ...state.words.find((word) => word.id === id) };

    foundWord.progress++;
    foundWord.lastEncounterDate = Date.now();

    state.words = [...state.words.filter((word) => word.id !== id), foundWord];
  }),
  decrementWordProgress: action((state, id) => {
    const foundWord = { ...state.words.find((word) => word.id === id) };

    if (foundWord.progress > 0) {
      foundWord.progress--;
      foundWord.lastEncounterDate = Date.now();
      state.words = [...state.words.filter((word) => word.id !== id), foundWord];
    }
  }),
  refreshLastEncounterDate: action((state, id) => {
    const foundWord = { ...state.words.find((word) => word.id === id), lastEncounterDate: Date.now() };
    state.words = [...state.words.filter((word) => word.id !== id), foundWord];
  }),
};

export const store = createStore<StoreModel>(
  persist(storeModel, {
    storage,
  }),
);

const typedHooks = createTypedHooks<StoreModel>();

export const useStoreActions = typedHooks.useStoreActions;
export const useStoreDispatch = typedHooks.useStoreDispatch;
export const useStoreState = typedHooks.useStoreState;
